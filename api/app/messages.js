const express = require('express');
const fileWork = require('./fileWork');
const router = express.Router();

router.get('/', (req, res) => {
    const messages = fileWork.getMessage();
    res.send(messages);
});

router.post('/', (req, res) => {
    if (req.body.message && req.body.author) {
        fileWork.addMessage(req.body);
        res.send('ok');
    } else {
        res.status(400).send('Input ERROR, write name or message');
    }
});


module.exports = router;