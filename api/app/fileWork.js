const fs = require('fs');
const {nanoid} = require('nanoid');

const fileName = 'file.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContent = fs.readFileSync(fileName);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },
    getMessage() {
        try {
            const fileData = JSON.parse(fs.readFileSync(fileName)).reverse();
            if (fileData.length <= 30) {
                return fileData;
            } else {
                const newResponse = [];
                fileData.forEach((mes, index) => {
                    if (index < 30) newResponse.push(mes);
                });
                return newResponse;
            }
        } catch (e) {
            return data;
        }
    },
    addMessage(message) {
        message.id = nanoid();
        data.push(message);
        this.save();
    },
    save() {
        fs.writeFileSync(fileName, JSON.stringify(data, null, 3));
    }
};
