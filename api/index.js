const express = require('express');
const cors = require('cors');
const messages = require('./app/messages');
const fileWork = require('./app/fileWork');

const app = express();

app.use(express.json());
app.use(cors());

const port = 8000;


fileWork.init();

app.use('/messages', messages);

app.listen(port, () => {
    console.log('We are on port ' + port);
});