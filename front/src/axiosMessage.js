const axios = require("axios");

const axiosMessage = axios.create({
    baseURL: 'http://localhost:8000'
});

export default axiosMessage;