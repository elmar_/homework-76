import React from 'react';
import Container from "@material-ui/core/Container";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import MessageContainer from "./containers/MessageContainer/MessageContainer";


const App = () => {

    return (
        <div>
            <CssBaseline/>
            <header>
                <AppToolbar />
            </header>
            <main>
                <Container maxWidth="md">
                    <MessageContainer />
                </Container>
            </main>
        </div>
    );
};

export default App;