import React, {useEffect} from 'react';
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import List from "@material-ui/core/List";
import MessagesBlock from "../../components/MessagesBlock/MessagesBlock";
import FormBlock from "../../components/FormBlock/FormBlock";
import {useDispatch, useSelector} from "react-redux";
import {getMessages, sendMessage} from "../../store/actions";
import ErrorMessage from "../../components/ErrorMessage/ErrorMessage";

const useStyles = makeStyles(theme => ({
    chatContainer: {
        minHeight: '60vh',
        maxHeight: '60vh',
        marginBottom: theme.spacing(3),
        overflowY: 'auto',
        padding: '10px 20px'
    }
}));


const MessageContainer = () => {

    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages);
    const error = useSelector(state => state.errorPost);
    const styles = useStyles();


    useEffect(() => {
        dispatch(getMessages());
        setInterval(() => {
            dispatch(getMessages());
        }, 3000);
    }, [dispatch]);


    const sendingMessage = data => {
        const datetime = new Date().toISOString();

        dispatch(sendMessage({
            ...data,
            datetime
        }));
    };

    return (
        <>
            <Paper variant="elevation" elevation={5} className={styles.chatContainer}>
                <List>
                    {messages.map(mes => (
                        <MessagesBlock data={mes} key={mes.id} />
                    ))}
                </List>
            </Paper>
            {error ? <ErrorMessage /> : null}
            <FormBlock sendMessage={sendingMessage} />
        </>
    );
};

export default MessageContainer;