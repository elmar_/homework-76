import axiosMessage from "../axiosMessage";

export const MESSAGE_SEND_SUCCESS = 'MESSAGE_SEND_SUCCESS';
export const MESSAGE_SEND_ERROR = 'MESSAGE_SEND_ERROR';

export const GET_MESSAGE_SUCCESS = 'GET_MESSAGE_SUCCESS';
export const GET_MESSAGE_ERROR = 'GET_MESSAGE_ERROR';

export const messageSendSuccess = () => ({type: MESSAGE_SEND_SUCCESS});
export const messageSendError = () => ({type: MESSAGE_SEND_ERROR});

export const getMessageSuccess = data => ({type: GET_MESSAGE_SUCCESS, data});
export const getMessageError = error => ({type: GET_MESSAGE_ERROR, error});

export const sendMessage = message => {
    return async dispatch => {
        try {
            await axiosMessage.post('/messages', message);
            dispatch(messageSendSuccess());
        } catch (e) {
            dispatch(messageSendError());
        }
    };
};

export const getMessages = () => {
    return async dispatch => {
        try {
            const response = await axiosMessage.get('/messages');
            dispatch(getMessageSuccess(response.data));
        } catch (e) {
            dispatch(getMessageError(e));
        }
    };
};

