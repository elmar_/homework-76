import {GET_MESSAGE_ERROR, GET_MESSAGE_SUCCESS, MESSAGE_SEND_ERROR, MESSAGE_SEND_SUCCESS} from "./actions";

const initialState = {
    messages: [],
    errorPost: false,
    errorGet: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MESSAGE_SUCCESS:
            return {...state, errorGet: {}, messages: action.data};
        case GET_MESSAGE_ERROR:
            return {...state, errorGet: action.error};
        case MESSAGE_SEND_ERROR:
            return {...state, errorPost: true};
        case MESSAGE_SEND_SUCCESS:
            return {...state, errorPost: false};
        default:
            return state;
    }
};

export default reducer;