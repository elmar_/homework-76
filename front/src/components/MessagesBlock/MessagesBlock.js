import React from 'react';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";

const MessagesBlock = ({data}) => {
    return (
        <>
            <ListItem>
                <ListItemText
                    primary={data.author}
                    secondary={data.message}
                />
            </ListItem>
            <Divider variant="middle" />
        </>
    );
};

export default MessagesBlock;