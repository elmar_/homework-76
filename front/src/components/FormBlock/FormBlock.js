import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const FormBlock = ({sendMessage}) => {
    const [message, setMessage] = useState({
        author: '',
        message: ''
    });

    const changingInput = e => {
        const {name, value} = e.target;

        setMessage(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    return (
        <Grid container direction="column" spacing={2}>
            <Grid item>
                <TextField
                    fullWidth
                    label="Author"
                    name="author"
                    variant="outlined"
                    value={message.author}
                    onChange={changingInput}
                />
            </Grid>
            <Grid item>
                <TextField
                    fullWidth
                    label="Message"
                    multiline
                    rows={3}
                    variant="outlined"
                    name="message"
                    onChange={changingInput}
                    value={message.message}
                />
            </Grid>
            <Grid item>
                <Button
                    type="button"
                    color="primary"
                    variant="contained"
                    onClick={() => (sendMessage({...message}, setMessage({...message, message: ''})))}
                >
                    Send
                </Button>
            </Grid>
        </Grid>
    );
};

export default FormBlock;