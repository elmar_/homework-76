import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles(theme => ({
    staticToolbar: {
        marginBottom: theme.spacing(2)
    }
}));


const AppToolbar = () => {
    const classes = useStyles();

    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Container maxWidth="lg">
                        <Typography variant="h6">
                            Chat
                        </Typography>
                    </Container>
                </Toolbar>
            </AppBar>
            <Toolbar className={classes.staticToolbar} />
        </>
    );
};

export default AppToolbar;