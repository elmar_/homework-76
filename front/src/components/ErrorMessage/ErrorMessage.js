import React from 'react';
import {Alert} from '@material-ui/lab';


const ErrorMessage = () => {
    return (
        <Alert severity="error">
            Input ERROR: Author or message is empty
        </Alert>
    );
};

export default ErrorMessage;